#include <stdio.h>
#include <mpi.h>

int main() {

	int ID, nproc;
	MPI_Init (NULL,NULL);
	MPI_Comm_rank(MPI_COMM_WORLD,&ID);
	MPI_Comm_size(MPI_COMM_WORLD,&nproc);

	int soma=ID, aux=0;
	
	if (ID<nproc-1){

		MPI_Recv(&aux,1,MPI_INT,ID+1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		soma+=aux;
	}

	if (ID>0)
		MPI_Send(&soma,1,MPI_INT,ID-1,0,MPI_COMM_WORLD);

	if (ID==0)
		printf("soma = %d\n",soma);

	MPI_Finalize();
	return 0;
}
