#include <stdio.h>
#include <mpi.h>

int main() {

	int ID, nproc;
	MPI_Init (NULL,NULL);
	MPI_Comm_rank(MPI_COMM_WORLD,&ID);
	MPI_Comm_size(MPI_COMM_WORLD,&nproc);

	int tag =0;

	MPI_Status status;

	if (ID!=0){

		MPI_Send(&ID,1,MPI_INT,0,tag,MPI_COMM_WORLD);

	}

	else{

        fprintf(stderr,"Hello do processo %d do total %d\n",ID, nproc);
        int ID_externo;


        for(int i=1;i<nproc;i++){

            MPI_Recv(&ID_externo,1,MPI_INT,MPI_ANY_SOURCE,tag,MPI_COMM_WORLD,&status);
            fprintf(stderr,"Hello do processo %d do total %d | %d %d \n", ID_externo, nproc, status.MPI_SOURCE, status.MPI_TAG);
        }
    }

    MPI_Finalize();
    return 0;
}
