#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


double f (double x) {

	return (x*x*x);

}

double trap (double a, double b, int n, double dx){

	double soma = (f(a)+f(b))/2; // values of extreme of function
	int i=0;
	
	for (i=1; i<n; i++)
		soma +=f(a+i*dx);

	soma*=dx;

	return soma;

}


int main (int argc, char **argv) {

	double a=0,b=10, dx;
	int n = 1024;

	if(argc==4){ 
		n = atoi(argv[3]); 
		a = atof(argv[1]);
		b = atof(argv[2]);
	}

	int ID,nproc;

	MPI_Init(NULL,NULL);
	MPI_Comm_rank(MPI_COMM_WORLD,&ID);
	MPI_Comm_size(MPI_COMM_WORLD,&nproc);

	dx = (b-a)/n;
	int nlocal = n/nproc;
	int resto = n%nproc;

	double local_a = a + ID*(nlocal)*dx;

/*caso o numero de pontos não seja multiplo do numero de processos cabera ao processo de ID==nproc-1*/
/*a soma do trecho "restante" da função. Ou seja, o ultimo processo será incumbido de fazer a soma*/
/*num trecho com maior número de pontos que os demais processos, por isso seu nlocal e local_b são alterado.*/

	if (resto != 0 && ID == nproc-1) 
		nlocal = (n/nproc)+resto;


	double local_b = local_a + (nlocal)*dx; 

	double soma = trap(local_a,local_b,nlocal,dx);

	double soma_total=0;


	MPI_Allreduce(&soma,&soma_total,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); 

	if (ID == 0)
		fprintf(stderr,"\nA integral de x^3 de %.2f até %.2f é = %.3f\n",a,b, soma_total);

	MPI_Finalize();
}
