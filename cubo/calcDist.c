#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef PI
#define PI M_PI
#endif

float toRadians(float degree){

    float r = degree * PI / 180;
    return r;
} 

float distVicenty(float lon1, float lat1, float lon2, float lat2) {

    float a = 6378137, b = 6356752.314245, f = 1 / 298.257223563;
    float L = toRadians(lon2 - lon1);


    float U1 = atan((1 - f) * tan(toRadians(lat1)));
    float U2 = atan((1 - f) * tan(toRadians(lat2)));
    float sinU1 = sin(U1), cosU1 = cos(U1);
    float sinU2 = sin(U2), cosU2 = cos(U2);
    float cosSqAlpha;
    float sinSigma;
    float cos2SigmaM;
    float cosSigma;
    float sigma;

    float lambda = L, lambdaP, iterLimit = 100;

    do{

        float sinLambda = sin(lambda), cosLambda = cos(lambda);
        sinSigma = sqrt((cosU2 * sinLambda)
                    * (cosU2 * sinLambda)
                            + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
                                * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
                            );
        if (sinSigma == 0) 
        {
            return 0;
        }

        cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
        sigma = atan2(sinSigma, cosSigma);
        float sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
        cosSqAlpha = 1 - sinAlpha * sinAlpha;
        cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;

        float C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
        lambdaP = lambda;
        lambda =L + (1 - C) * f * sinAlpha
                    *(sigma + C * sinSigma
                        *(cos2SigmaM + C * cosSigma
                            *(-1 + 2 * cos2SigmaM * cos2SigmaM)
                            )
                        );

    } while (abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);

    if (iterLimit == 0) 
        return 0;


    float uSq = cosSqAlpha * (a * a - b * b) / (b * b);
    float A = 1 + uSq / 16384
            * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
    float B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
    float deltaSigma = 
                B * sinSigma
                    * (cos2SigmaM + B / 4
                        * (cosSigma 
                            * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM
                                * (-3 + 4 * sinSigma * sinSigma)
                                    * (-3 + 4 * cos2SigmaM * cos2SigmaM)));

    float s = b * A * (sigma - deltaSigma);

    return (s/1000);
}


float distHaversine(float lon1, float lat1, float lon2, float lat2){

    int R = 6371;
    float diflon=0, diflat=0, a=0, c=0, distance=0;

    //calculo da distancia entre os dois eventos em radianos
    diflon = toRadians(lon1 - lon2);
    diflat = toRadians(lat1 - lat2);

    //formula de Haversine
    a = (sin(diflat/2)*sin(diflat/2)) + cos(toRadians(lat1)) * cos(toRadians(lat2)) * (sin(diflon/2)*sin(diflon/2));
    c = 2 * asin(sqrt(a));
    distance = R*c;

    return (distance);

}



