#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "cubeHeader.h"
#include <omp.h>

int writeFile(Profile *line, float *minmax){

    //minmax = (minlon, maxlon, minlat,maxlat,minz.maxz)
    printf("Escrevendo perfil do arquivo: out_cube");

    int i,j;
    FILE *saida;
    saida = fopen("out_cube.txt","w");

    fprintf(saida,"DSAA\n");
    fprintf(saida,"%d %d\n",nz,npoints);
    fprintf(saida,"%d %d\n",line[0].records->dist_first,line[npoints].records->dist_first);
    fprintf(saida,"%d %d\n",zmin,zmax); 
    fprintf(saida,"%f %f\n",minmax[4],minmax[5]);

    for(i=0;i<nz;i++){
        for(j=0;j<npoints;j++)
            fprintf(saida,"%.4f ",line[j].records->z[i][2]);
        fprintf(saida,"\n");
    }

    fclose(saida);

}

