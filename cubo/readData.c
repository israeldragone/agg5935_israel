#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "cubeHeader.h"
#include "calcDist.h"
#include <omp.h>


int readProfile(Profile **line){

    FILE *fio;
    Profile *profile = NULL;
    char data[1024];
    int i=0;
    printf("Reading profile\n");

    profile = calloc(npoints,sizeof(Profile));
    
    if (profile == NULL) {
        printf("profile null, bad allocation");
        return 1;
    }

   if ( (fio = fopen("profile.txt","r")) == NULL){ // checa abertura do arquivo
        fprintf(stdout,"Arquivo Invalidoa\n");
        return 1;
        }

    while ( ! feof(fio) ) {
        if ((fgets(data, 1024, fio)) == NULL) continue; // se nao for possivel ler uma linha, pula para a proxima

        sscanf(data,"%f %f %d", &profile[i].records->lon,&profile[i].records->lat,&profile[i].records->dist_first);
        i++;

    }

    if (fio != NULL)
        fclose(fio);

    (*line)=profile;

    return 0;
}


int readCube(Profile *line, char *choose, float (*metodo)(float, float, float, float)){

    FILE *fio;
    char data[1024];
    int i=0,h=0,cont=0;
    float dist;
    float depth, lon, lat, dvsa, vsref, dvsp, vabs, vsmean, dvsm;


    if ( (fio = fopen("SL2013sv_25k-0.5d.mod","r")) == NULL){ // checa abertura do arquivo
        fprintf(stdout,"Arquivo Invalido\n");
        return 1;
        }

    printf("Reading cube file\n");

    while ( ! feof(fio) ) {
        if ((fgets(data, 1024, fio)) == NULL) continue; // se nao for possivel ler uma linha, pula para a proxima
        if (data[0] == '#') continue;

        sscanf(data,"%f %f %f %f %f %f %f %f %f",&depth, &lon, &lat, &dvsa, &vsref, &dvsp, &vabs, &vsmean, &dvsm);

        int cp=(npoints/2);

        dist = metodo(line[cp].records->lon,line[cp].records->lat,lon,lat);

        if(dist > 1000) continue; //faz um filtro excluindo todos os pontos que distam mais de 1000 km do ponto central do perfil
        
        h = (depth-zmin)/dz; //calcula a posicao do vetor para tal profundide

/*#pragma omp parallel for num_threads(4) private(dist,h,i)*/
        for (i=0;i<npoints;i++){

            dist = metodo(line[i].records->lon,line[i].records->lat,lon,lat);

            if(dist < maxdist){
                printf("%d\n",cont);
                line[i].records->z[h][0]+=dvsp;
                line[i].records->z[h][1]+=1;
                cont++;
            }//fecha if

        }//fecha for
    }//fecha while

    if (fio != NULL)
        fclose(fio);

    return 0;

}


