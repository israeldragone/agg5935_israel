#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "cubeHeader.h"
#include <omp.h>


int calcVeloMedia(Profile *line){

    int i,j;
    printf("Calculating average velocity\n");

/*#pragma omp parallel for num_threads(4)*/
    for(i=0;i<npoints;i++){
        for(j=0;j<nz;j++)

            line[i].records->z[j][2]=(line[i].records->z[j][0]/line[i].records->z[j][1]);
    }

    return 0;
}

float calMinMax(Profile *line,float *minmax){

    printf("Calculating min and max values");

    int i,j;

    float minlon=999999,minlat=999999,minz=999999,maxlon=-999999,maxlat=-999999,maxz=0;

    for(i=0;i<npoints;i++){

        if(line[i].records->lon < minlon)
            minlon=line[i].records->lon;

        if(line[i].records->lat < minlat)
            minlat=line[i].records->lat;

        if(line[i].records->lon > maxlon)
            maxlon=line[i].records->lon;

        if(line[i].records->lat > maxlat)
            maxlat=line[i].records->lat;

        for(j=0;j<nz;j++){

            if(line[i].records->z[j][2] < minz)
                minz=line[i].records->z[j][2];

            if(line[i].records->z[j][2] > maxz)
                maxz=line[i].records->z[j][2];

        }
    }

    minmax[0]=minlon;
    minmax[1]=maxlon;
    minmax[2]=minlat;
    minmax[3]=maxlat;
    minmax[4]=minz;
    minmax[5]=maxz;

    return 0;

}

