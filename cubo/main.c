#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <string.h>
#include "cubeHeader.h"
#include "calcDist.h"
#include "readData.h"
#include "aux.h"
#include "write.h"



int main(int argc, char **argv){

    clock_t tic = clock();
    float minmax[6]; //(minlon, maxlon, minlat,maxlat,minz.maxz)
    char choose[2];

    if (argc == 2)
        strcpy(choose,argv[1]);
    else
        printf("Execute ./cubo -h (haversine) ou ./cubo -v (vicenty) para escolher o metodo do calculo de distancia\n");

    Profile *line = NULL;
    float (*metodo) (float, float, float, float);
/*    int i,j;*/

    if (strcmp(choose,"-h") == 0)
         metodo=distHaversine;
    else if (strcmp(choose,"-v") == 0)
        metodo=distVicenty;
    else{
        printf("Escolha entre as opcoes -h e -v\n");
        return 1;
    }

    readProfile(&line);
    
    readCube(line, choose, metodo);
    
    calcVeloMedia(line);

    calMinMax(line,minmax);

    writeFile(line,minmax);

/*    for (i=0;i<6;i++)*/
/*        printf("%f\n",minmax[i]);*/

/*    for(i=0;i<npoints;i++){*/
/*        for(j=0;j<nz;j++)*/

/*            printf("ponto %d prof %d vsmedio %f \n",i,j, line[i].records->z[j][1]);*/
/*    }*/

    free(line);
    
    clock_t toc = clock();

    printf("Elapsed: %f seconds\n", (float)(toc - tic) / CLOCKS_PER_SEC);
    printf("Profile extraction finalized, run ./veiw_profile out_cube to see the velocity profile\n\n");

    return 0;

}














