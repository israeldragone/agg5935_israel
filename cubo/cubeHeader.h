#define dz 25
#define zmin 25
#define zmax 700
#define nz (zmax-zmin)/dz
#define npoints 35
#define maxdist 100

#ifndef well
#define well

typedef struct WELL{
    float lon;
    float lat;
    int dist_first;
    float z[nz][3];
}Well; 

typedef struct PROFILE{
    Well records[npoints];
}Profile;

#endif
