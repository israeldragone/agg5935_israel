#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int chooseColumn(int *choice, float *col1, float *col2, float *col3, float *col4, float **col){

	// verifica a coluna escolhida para a media e aponta um vetor para a respectiva variavel

	int stop = 0;

	switch (*choice){
		case 1:
			*col = col1;
			break;
		case 2:
			*col = col2;
			break;
		case 3:
			*col= col3;
			break;
		case 4:
			*col = col4;
			break;
		default: 
			printf("execute: \n./prog -c 1 para a media da coluna 1\n");
			printf("./prog -c 2 para a media da coluna 2\n");
			printf("./prog -c 3 para a media da coluna 3\n");
			printf("./prog -c 4 para a media da coluna 4\n");
			stop = 1;
			break;
	} // fecha swicth

	return stop;

}

int readData(int *choice, float *col1, float *col2, float *col3, float *col4, float **col){

	FILE * fio;
	char data[1024], tag[1], w1[4], w2[5], snumber[4]; 
	float soma = 0;
	int cont = 0 ,aux = 0;


	if ( (fio = fopen("dados.txt", "r"))  == NULL) {  // verifica a abertura do arquivo
			fprintf(stdout, "Arquivo invalido\n");
			return 1; 
		}

		// le o arquivo ate chegar ao fim
		while ( ! feof(fio) ) {
			if ((fgets(data, 1024, fio)) == NULL) continue; // se nao for possivel ler uma linha, pula para a proxima

			if (data[0] == '>') { // verifica se o primeiro caracter da linha eh um >, indicando o cabecalho

				// como aux foi inicializado em 0 a primeira vez que for lido um cabecalho nao sera exibido a media, somente nas proximas n vezes
				if (aux == 1 ) 
					printf("A serie numero %s contem %d numeros. A media da coluna %d vale %+.3f\n",snumber, cont, *choice, soma/cont);

				sscanf(data,"%s %s %s %s", tag, w1, w2, snumber); // le cabecalho
				aux = 1;
				soma = 0; cont = 0; //zera a soma e o contador de linhas
			}

			else { // se nao for cabecalho le os dados
				sscanf(data,"%f %f %f %f", col1, col2, col3, col4);
				soma += **col; //incrementa a variavel soma com o valor da coluna escolhida na execucao
				cont++; //incrementa o numero de valores lidos
			}

		} //fecha while

		if (fio != NULL) 
			fclose(fio);

	return 0;
}

int main(int argc, char **argv) {

	int choice = 0, stop = 0;
	float c1 = 0, c2 = 0, c3 = 0, c4 = 0, *column = NULL;

	// verifica se a chamada da funcao esta compativel com: ./prog -c X, observe que o X é testado somente na funcao chooseColumn dentro da estrutura switch case

	if ( (argc != 3) || ( strcmp(argv[1], "-c") != 0 ) ){
		printf("execute: ./prog -c 1 para a media da coluna 1\n");
		return 1;
	}

	// parsing
	choice = atoi(argv[2]);

	// chama a funcao para avaliar a escolha da coluna para o calculo da media, a funcao retorna 1 caso a opcao seja invalida o que encerra a execucao no if a seguir

	stop = chooseColumn(&choice, &c1, &c2, &c3, &c4, &column);

	if (stop == 1) return 1;

	// se tudo foi bem ate aqui, a funcao para ler o arquivo eh acionada. Como os dados nao sao salvos, a medida que sao lidos eh feita o calculo 

	readData(&choice, &c1, &c2, &c3, &c4, &column);



	return 0;
}
