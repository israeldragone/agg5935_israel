#include <stdio.h>

void muda (float *a){
	if (a == NULL){
		printf("Não recebi nada\n");
		return;
	}
	*a=2.;
	return 0;
}

int main(){
	float a;
	float *ap;
	
	//mudar o valor a usando ap
	ap = &a;
	muda(&ap);
	
	return 0;
}
