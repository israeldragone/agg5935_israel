#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

const double G = 6.67E-11;
const double ANO = 365.*24.*3600.;
const double UA = 149597870700.; 

typedef struct corpo{
	double vx;
	double vy;
	double px;
	double py;
	double px2;
	double py2;
	double M;
} Corpo;


double randNorm();

void init(long n, Corpo *C);

void print_orbita(FILE *fo, long n, double t, Corpo *C);

void gravidade(long n, double dt,double t, Corpo *C, int nth);



int main(int argc, char** argv)
{
	double t1=0, t2=0, exec_time=0;

	t1 = omp_get_wtime();
	
	Corpo *C1;
	
	long n = atoi(argv[2]); //numero de corpos
	int nth = atoi(argv[1]); //numero de threads
	
	
	C1 = (Corpo *) malloc(sizeof(Corpo)*n);
	
	init(n,C1);
	

	double t=0,taux=0;
	double dt = ANO/10000;

	FILE *fo;
	char ss[100];
	sprintf(ss,"orbitas.txt");
	fo = fopen(ss,"w");
	fprintf(fo,"%ld\n",n);
	
	
	print_orbita(fo, n, t, C1);
	
/*#	pragma omp parallel num_threads(nth) private(t,dt,taux)*/

	for (t=dt, taux=dt; t<=ANO*1.0; t+=dt,taux+=dt){
		
		gravidade(n,dt,t,C1,nth);
	
		if (taux>=ANO/200) {
			taux=0.0;
			print_orbita(fo, n, t, C1);
		}
		
	}
	
	free(C1);

	t2 = omp_get_wtime();
	exec_time = t2-t1;

	printf("Execution time = %lf seconds\n",exec_time);
	return (0);
}

double randNorm()
{
	return(2.0*((double)rand()/RAND_MAX-0.5));
}


void print_orbita(FILE *fo, long n, double t, Corpo *C)
{
	long i;
	
	fprintf(fo,"%f ",t/ANO);
	
	for (i=0;i<n;i++){
		fprintf(fo,"%.3f %.3f ",C[i].px/UA,C[i].py/UA);
	}
	fprintf(fo,"\n");
	
}

//nao precisa mecher
void init(long n, Corpo *C){
	long i;
	
	double x,y;
	double ra, va;
	
	srand(1);
	
	for (i=0; i<n; i++){
		x = randNorm()*200E9;
		y = randNorm()*200E9;
		C[i].px = x;
		C[i].py = y;
		
		ra = sqrt(x*x+y*y);
		
		va = 30000.0*sqrt(150.E9/ra);
		
		C[i].vx = -va*y/ra;//randNorm()*10000;
		C[i].vy = va*x/ra;//randNorm()*10000;
		
		C[i].M = fabs(randNorm()*1E23);
		
	}
	
	//sol
	C[0].vx = 0;
	C[0].vy = 0;
	C[0].px = 0;
	C[0].py = 0;
	C[0].M = 2.0E30;
	
	//terra
	C[1].vx = 0;
	C[1].vy = 30000.0;
	C[1].px = UA;
	C[1].py = 0;
	C[1].M = 6.0E27;
	
	//lua
	if (n>2){
		C[2].vx = 0;
		C[2].vy = 30000.0 + 6800.0;
		C[2].px = UA+400000000.0*15;
		C[2].py = 0;
		C[2].M = 7.0E22;
	}
	
}

void gravidade(long n, double dt, double t, Corpo *C,int nth){
	

#	pragma omp parallel num_threads(nth)
{
	long i,j;
	double ax,ay,dx,dy,raux,r3;

#	pragma omp for
	for (i=0;i<n;i++){
		ax = 0;
		ay = 0;

		//determine a aceleracao ax e ay
		for (j=0;j<n;j++){
		
			if (i == j) continue; 

			dx= (C[i].px-C[j].px);
			dy= (C[i].py-C[j].py);
			
			raux = sqrt( dx*dx + dy*dy );
			r3 = (raux*raux*raux);
			
			ax += -(G*C[j].M*dx) / r3;
			ay += -(G*C[j].M*dy) / r3;

		}
		
		
		//atualiza a velocidade considerando que ela esteja meio passo de tempo deslocado do tempo
		if (t==dt){
			C[i].vx += ax*dt/2;
			C[i].vy += ay*dt/2;
		}
		else {
			C[i].vx += ax*dt;
			C[i].vy += ay*dt;
		}
		
		//atualize os valores de px2 e py2
		C[i].px2 = C[i].px + C[i].vx*dt;
		C[i].py2 = C[i].py + C[i].vy*dt;
	}


#	pragma omp for 
	for (i=0;i<n;i++){
		C[i].px=C[i].px2;
		C[i].py=C[i].py2;
	}
}//fecha pragma

}//fecha funcao

