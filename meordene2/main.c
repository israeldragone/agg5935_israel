#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

/*
 * Run with:
 * wget -q -O - wget -q -O - 'http://www.moho.iag.usp.br/fdsnws/event/1/query?format=text&limit=10' | ./main -lat
 */

typedef struct EVENT {
	char *id;
	char *date;
	float latitude;
	float longitude;
	float depth;
} Event;

int readone(FILE *fio, Event *e) {
	/*
	 * Le uma linha, retorna:
	 * 
	 * 1  = Quando lido
	 * 0  = Se a linha for comentário
	 * -1 = Se o arquivo acabar
	 * -2 = Se não tem os campos esperados
	 */
	
	char line[1024];
	
	char *s_date, *s_id, *s_lat, *s_lon, *s_depth;
	float lat, lon, depth;
	
	if ((fgets(line, 1024, fio)) == NULL) return -1;
	if (line[0] == '#') return 0;
	
	s_id   = strtok(line, "|");
	s_date = strtok(NULL, "|");
	s_lat  = strtok(NULL, "|");
	s_lon  = strtok(NULL, "|");
	s_depth= strtok(NULL, "|");
	
	if (s_id == NULL || s_date == NULL || s_lat == NULL || s_lon == NULL || s_depth== NULL) return -2;
	
	e->id        = strdup(s_id);
	e->date      = strdup(s_date);
	e->latitude  = atof(s_lat);
	e->longitude = atof(s_lon);
	e->depth     = atof(s_depth);
		
	return 1;
}

void show(Event *es, int n) {
	int i;
	for(i = 0; i < n; i++) {
		fprintf(stderr, "%s %s %f %f %f\n",
				es[i].id,
				es[i].date,
				es[i].latitude,
				es[i].longitude,
				es[i].depth
		);
	}
}

int bylat(const void *a, const void *b) {

	Event *ae = (Event *) a;
	Event *be = (Event *) b;
	
	return (ae->latitude > be->latitude ? 1 : -1 );

}

int bydep(const void *a, const void *b) {

	Event *ae = (Event *) a;
	Event *be = (Event *) b;
	
	return (ae->depth > be->depth ? 1 : -1 );

}


int bylong(const void *a, const void *b) {

	Event *ae = (Event *) a;
	Event *be = (Event *) b;
	
	return (ae->longitude > be->longitude ? 1 : -1 );

}

int main(int argc, char **argv)
{
	Event es[11];
	Event e;
	int err, i = 0;
	
	while ( (err = readone(stdin, &e)) >= 0) {
		if (err == 1){
			es[i] = e;
			i ++;
		}
	}	
	
	fprintf(stderr, "Before Sort:\n");
	show(es, i);

	if (strncmp("-lat", argv[1], 4) == 0) {
		qsort(es, i, sizeof(Event),bylat);
	} else if (strncmp("-lon", argv[1], 4) == 0) {
		qsort(es, i, sizeof(Event),bylong);
	} else if (strncmp("-dep", argv[1], 4) == 0) {
		qsort(es, i, sizeof(Event),bydep);
	} else {
		fprintf(stderr, "No sort will be performed !\n");
	}
	
	fprintf(stderr, "After Sort:\n");
	show(es, i);
	return 0;
}
