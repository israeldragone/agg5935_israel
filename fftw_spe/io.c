#include <stdio.h>
#include <stdlib.h>
#include <sachead.h>
#include <sacheadEnums.h>

/** 
 * This method is responssible for freeing up memory allocated by io_readSacHead() and io_readSacData() methods.
 * 
 * @param p 
 *  Pointer for the data to be alocated.
 * @return 
 *  Always return NULL to be re-atributed to the pointer.
 */
void *io_freeData(void *p)
{
	if (p != NULL) {
		//    fprintf(stderr,"Liberando . . . ");
		free(p);
	}
	return NULL;
}

/** 
 * Reads the header part of a SAC file. The header can be used later for reading the data part in method io_readSacData()
 * 
 * @param filename Name of file to read
 * 
 * @return A newly SACHEAD data for the given file
 */
SACHEAD *io_readSacHead(char *filename)
{
	FILE *ent;
	int i;
	SACHEAD *h;

	h = NULL;
	h = malloc(sizeof(SACHEAD));
	if (h == NULL)
		return NULL;

	ent = fopen(filename, "r");
	if (ent == NULL)
		return NULL;
	i = fread(h, sizeof(SACHEAD), 1, ent);
	fclose(ent);

	if (i != 1 || h->nvhdr != 6) {
		io_freeData(h);
		return NULL;
	} else {
		return h;
	}
}

/** 
 * Read the data part of the SAC file.
 * 
 * @param filename The name of the SAC file
 * @param h
 *  The header of the SAC file pre-readed with the io_readSacHead().
 * @return 
 *  The newly allocated vector of floating points for the data readed from the SAC file.
 */
float *io_readSacData(char *filename, SACHEAD * h)
{
	FILE *ent;
	float *y = NULL;
	int i, dataspam;

	switch (h->iftype) {
	case (ITIME):
		if (h->leven == SAC_TRUE)
			dataspam = 1;
		else
			dataspam = 2;
		break;

	case (IRLIM):
		dataspam = 2;
		break;

	case (IAMPH):
		dataspam = 2;
		break;

	case (IXY):
		if (h->leven == SAC_TRUE)
			dataspam = 1;
		else
			dataspam = 2;
		break;

	case (IXYZ):
		fprintf(stderr, "Sac file type IXYZ is not supported.");
		return NULL;
		break;
	}

	if (dataspam == 0)
		return NULL;

	y = malloc(sizeof(float) * dataspam * h->npts);
	if (y == NULL)
		return NULL;

	ent = fopen(filename, "r");
	fseek(ent, SAC_HEADER_SIZE, 0);
	i = fread(y, sizeof(float), dataspam * h->npts, ent);
	fclose(ent);

	if (i != dataspam * h->npts) {
		io_freeData(y);
		return NULL;
	} else {
		return y;
	}
}

/** 
 * Read the header and data part in one call. Use this to read the full file to memory.
 * 
 * @param filename The name of the SAC file to read.
 * @param h The newly readed header part of the sac file.
 *  Note: This is an output parameter, you should pass in a pointer to a pointer in this parameter (like &h, where h is of type SACHEAD*).
 * @return The newly readed data part of the sac file.
 */
float *io_readSac(char *filename, SACHEAD ** h)
{
	SACHEAD *ph = NULL;
	float *py = NULL;

	ph = io_readSacHead(filename);
	if (ph == NULL)
		return NULL;

	py = io_readSacData(filename, ph);
	if (py == NULL) {
		io_freeData(ph);
		return NULL;
	}

	(*h) = ph;
	return py;
}

