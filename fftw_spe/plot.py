import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib import gridspec
import sys

def loadseismogram(filename):
	a = open(filename, "r")
	delta = np.fromfile(a, dtype=np.float32, count=1)
	a.seek(5*4*16-4,0)
	npts = np.fromfile(a, dtype=np.int32,count=1)
	a.seek(158*4,0)
	z=np.fromfile(a, dtype=np.float32)
	a.close()
	return np.arange(npts[0])*delta, z

def loadspectrogram():
	x = np.fromfile("X", dtype=np.float32)
	y = np.fromfile("Y", dtype=np.float32)
	z = np.fromfile("Z", dtype=np.complex64)
	z = np.reshape(z, (x.shape[0],y.shape[0]))
	z = np.abs(z.transpose())
	return x,y,z

def preparearea():
	mpl.rcParams['toolbar'] = 'None'
	fig = plt.figure(figsize=(12, 5)) 
	gs = gridspec.GridSpec(2, 1, height_ratios=[1, 4]) 
	ax0 = plt.subplot(gs[0])
	ax1 = plt.subplot(gs[1])
	return fig, ax0, ax1

def draw(data, xmin = None, xmax = None, scale = None):
	if data is not None:
		(t,a,x,y,z,p1,p2,fig) = data
		p1.plot(t, a, lw=0.5)
		qm = p2.imshow(
			np.abs(z),
			aspect='auto',
			cmap=plt.cm.Purples,
			norm=colors.Normalize(
				vmin=np.min(z),
				vmax=np.max(z)/150.
			),
			origin='lower',
			extent=[np.min(t),np.max(t),np.min(y),np.max(y)]
		)
		draw.data = (t,a,x,y,z,p1,p2,fig,qm) 
	
	(t,a,x,y,z,p1,p2,fig,qm) = draw.data
	
	if scale is not None:
		qm.set_norm(
			colors.Normalize(
				vmin=np.min(z),
				vmax=np.max(z)/scale
			)
		)
		fig.canvas.draw()
		return
	
	xmin = xmin if xmin is not None else np.min(t)
	xmax = xmax if xmax is not None else np.max(t)
	ff = (t > xmin) & (t < xmax)
	
	p1.set_xlim([xmin, xmax])
	p1.set_ylim([np.min(a[ff]), np.max(a[ff])])
	p2.set_xlim([xmin, xmax])
	fig.canvas.draw()

def on_key(event):
	if 'scale' not in on_key.__dict__:
		on_key.scale = 150.
	
	if event.key == 'up':
		on_key.scale *= 2
		draw(None, None, None, on_key.scale)
	
	if event.key == 'down':
		on_key.scale /= 2 if on_key.scale >= 2 else 1
		draw(None, None, None, on_key.scale)
	
	if event.key in ['q','Q']:
		sys.exit()

def onclick(event):
	if 'xmin' not in onclick.__dict__:
		onclick.xmin = None
		onclick.xmax = None
		onclick.first = False
	
	if event.button == 1:
		if onclick.first is False:
			onclick.xmin = event.xdata
			onclick.first = True
			return
		else:
			onclick.xmax = event.xdata
			onclick.first = False
			p1.set_xlim([onclick.xmin, onclick.xmax])
			draw(None, onclick.xmin, onclick.xmax)
	else:
		onclick.first = False
		draw(None, None, None)

if __name__ == "__main__":
	t, a        = loadseismogram(sys.argv[1])
	x, y, z     = loadspectrogram()
	fig, p1, p2 = preparearea()
	
	draw((t,a,x,y,z,p1,p2,fig),np.min(x), np.max(x))
	_ = fig.canvas.mpl_connect('button_press_event', onclick)
	_ = fig.canvas.mpl_connect('key_press_event', on_key)
	plt.show()

