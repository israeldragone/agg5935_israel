#include <sachead.h>

/// Read a full SAC file, header and data
float *io_readSac(char *filename, SACHEAD ** h);

/// Read the data part only of a SAC file
float *io_readSacData(char *filename, SACHEAD * h);

/// Read the header part of the SAC file
SACHEAD *io_readSacHead(char *filename);

/// Properly free the memory allocated under the pointer p
void *io_freeData(void *p);
