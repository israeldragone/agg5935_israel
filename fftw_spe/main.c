#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#include <math.h>

#include <complex.h>
#include <fftw3.h>

void demean_and_copy(float *in, int imin, int imax, float *out) {
	float mean = 0.0;
	int i = 0;
	
	for(i = imin; i <= imax; i++) mean += in[i];
	mean /= (imax - imin + 1);

	for(i = imin; i <=imax ; i++) out[i-imin] = in[i] - mean;
	
	return;
}

void compute(float *y, int npts, float dt, int halfwidth, int step) {
	float tmp;
	int imin, imax, ic;
	int i;
	
	/*
	 * 1.
	 * Definir os tipos das variáveis, vamos utilizar
	 * a versão single precision, com entrada 
	 * float e saída complex
	 */
    fftwf_plan direct_plan;
    fftwf_complex *out = NULL;
    float *window = NULL;
	
	fprintf(stderr, "halfwidth=%d step=%d\n",halfwidth, step);
	
	// Open output files
	FILE *X = fopen("X", "w"); // X value vector
	FILE *Y = fopen("Y", "w"); // Y value vector
	FILE *Z = fopen("Z", "w"); // Spectrogam values for each X,Y value
	
	/*
	 * 2.
	 * Alocar a memória necessária
	 */
    out    = fftwf_alloc_complex(2*halfwidth+1);
    window = fftwf_alloc_real(2*halfwidth+1);

	/* 3.
	 * Calcular e escrever os valores
	 * de frequencia para o arquivo em 
	 * binário
	 */
	for(i = 0; i < (halfwidth+1); i ++) {
        tmp = i*(1/((2*halfwidth+1)*dt));
		fwrite(&tmp, sizeof(float), 1, Y);
	}
	
	/* 4.
	 * Preparar o plano para realizar uma transformada
	 * 1d utilizando dados de "single precision" como
	 * entrada e retornando números complexos
	 */
    direct_plan = fftwf_plan_dft_r2c_1d(2*halfwidth+1, window, out, FFTW_MEASURE);

	/* 5.
	 * Escreva um laço pensando na variável ic como sendo
	 * o centro da janela que esta sendo utilizado. Lembre-se
	 * você já tem as variáveis:
	 * 
	 * halfwidth = meia largura da janela
	 * step      = passo da janela
	 * npts      = número de pontos
	 */
    for(ic = halfwidth; (halfwidth+ic) < (npts); ic+=step) {
		/* 6.
		 * Defini os índices da janela a ser transformada
		 */
        imin = ic-halfwidth;
        imax = ic+halfwidth;
		
		// Remove a média e obtém a cópia dos dados
		demean_and_copy(y, imin, imax, window);
		
		/* 7.
		 *  Calcula a FFT
		 */
        fftwf_execute(direct_plan);
		
		/* 8.
		 * Salve os dados para realizar a visualização
		 */
        fwrite(out, sizeof(fftwf_complex), halfwidth+1, Z);
		
		// Calcule o valor de X para o ponto central
		tmp = (float)(ic*dt);
		fwrite(&tmp, sizeof(float), 1, X);
	}

	// Close output files
	fclose(X);
	fclose(Y);
	fclose(Z);

	// Destroy plan and data & allocted memory
	fftwf_destroy_plan(direct_plan);
	fftw_free(out);
	fftw_free(window);

	window = NULL;
	
	return;
}

int main (int argc, char **argv) {
	/*
	 * Input Data handling
	 */
	SACHEAD *h;
	float *y = NULL;
	
	/*
	 * Load data
	 */
	y = io_readSac(argv[1], &h);
	if (y == NULL) {
		fprintf(stderr, "Failed to read data.");
		return 1;
	}
	fprintf(stderr, "Loaded %d points with dt=%.4f\n", h->npts, h->delta);
	
	/*
	 * Define Windows
	 */
	int halfwidth = atoi(argv[2]);
	int step      = atoi(argv[3]);
	
	compute(y, h->npts, h->delta, halfwidth, step);
	
	/*
	 * Free Up input data
	 */
	h = io_freeData(h);
	y = io_freeData(y);
	
	return 0;
}
