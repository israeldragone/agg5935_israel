#include <petsc/petscksp.h>

int main(int argc, char **argv){

	PetscInitialize(&argc,&argv,NULL,NULL);

	Vec x,y,u,v;
	int n=10;
	
	VecCreate(MPI_COMM_WORLD,&x);
	VecSetSizes(x,PETSC_DECIDE,n);
	VecSetFromOptions(x);
	
	VecSet(x,1.3);
	
	int xi,xf,i,j;
	float a=1.,b=2.,c=4.;
	VecGetOwnershipRange(x,&xi,&xf);
	PetscScalar val,ind;
	
	for (i=xi;i<xf;i++){
		val = (float)i;
		VecSetValues(x,1,&i,&val,INSERT_VALUES);
	}
	
	VecAssemblyBegin(x);
	VecAssemblyEnd(x);

	VecDuplicate(x,&y);
	VecDuplicate(x,&u);
	VecDuplicate(x,&v);
	
	VecAXPY(y,2.0,x);
	VecAXPY(u,1.0,x);
	VecAXPY(v,2.0,x);

	//f(x) = a*x^3 + b*x^2 +c
	VecPow(u,3); //x^3
	VecPow(v,2); //x^2
	VecAXPBY(u,a,b,v); //ax^3+bx^2
	VecShift(u,c); // ax^3+bx^2+c


	Mat A;
	MatCreate(MPI_COMM_WORLD,&A);
	MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,n,n);

	MatSetFromOptions(A);
	MatMPIAIJSetPreallocation(A,3,NULL,1,NULL);

	int Ai,Af;

	MatGetOwnershipRange(A,&Ai,&Af);

	PetscScalar v1=1.0,v2=-2.0,v3=1.0;

	for (i,j=Ai;i<Af;i++){

		if(i-1>=j){
			j=i-1;
			MatSetValues(A,1,&i,1,&j,&v1,ADD_VALUES);
		}

		if(i+1>=j){
			j=i-1;
			MatSetValues(A,1,&i,1,&j,&v3,ADD_VALUES);
		}

		MatSetValues(A,1,&i,1,&j,&v2,ADD_VALUES);
	}


	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	
	MatMult(A,x,y);
	
	KSP ksp;
	KSPCreate(MPI_COMM_WORLD,&ksp);
	KSPSetOperators(ksp,A,A);
	KSPSetFromOptions(ksp);
	
	KSPSolve(ksp,y,x);
	
	VecView(y,PETSC_VIEWER_STDOUT_WORLD);


	KSPDestroy(&ksp);
	VecDestroy(&x);
	VecDestroy(&y);
	MatDestroy(&A);

	PetscFinalize();
	return 0;

}
