#include <petsc/petscksp.h>

/*float f(x) {*/

/*	return (x*x*x*x);*/

/*}*/

int main(int argc, char **argv){

	double dpho = 2300;
	double g = 10;
	double dx = 2000;
	double E = 1.0E11;
	double T = 20.0E3;
	double ni = 0.25;
	double D = (E*T*T*T)/12*(1-(ni*ni));


	PetscInitialize(&argc,&argv,NULL,NULL);

	int n=200,pmedio=n/2;
	PetscScalar p = -1.0E13/dx;

	Vec x,y;

	VecCreate(MPI_COMM_WORLD,&x);
	VecSetSizes(x,PETSC_DECIDE,n);
	VecSetFromOptions(x);
	
	int xi,xf,i,j;
	float a=1.,b=2.,c=4.;
	VecGetOwnershipRange(x,&xi,&xf);
	
	VecSetValues(x,1,&pmedio,&p,INSERT_VALUES);
	
	VecAssemblyBegin(x);
	VecAssemblyEnd(x);

	VecDuplicate(x,&y);
	
	Mat A;
	MatCreate(MPI_COMM_WORLD,&A);
	MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,n,n);

	MatSetFromOptions(A);
	MatMPIAIJSetPreallocation(A,5,NULL,5,NULL);

	int Ai,Af;

	MatGetOwnershipRange(A,&Ai,&Af);

	PetscScalar v1=D,v2=-4*D;
	PetscScalar v3=6*D+dx*dx*dx*dx*dpho*g;

	for (i=Ai;i<Af;i++){

		if(i-2>=0){
			j=i-2;
			MatSetValues(A,1,&i,1,&j,&v1,ADD_VALUES);
		}
	
		if(i+2<n){
			j=i+2;
			MatSetValues(A,1,&i,1,&j,&v1,ADD_VALUES);
		}


		if(i+1<n){
			j=i+1;
			MatSetValues(A,1,&i,1,&j,&v2,ADD_VALUES);
		}

		if(i-1>=0){
			j=i-1;
			MatSetValues(A,1,&i,1,&j,&v2,ADD_VALUES);
		}


		MatSetValues(A,1,&i,1,&i,&v3,ADD_VALUES);
	}


	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	
	
	KSP ksp;
	KSPCreate(MPI_COMM_WORLD,&ksp);
	KSPSetOperators(ksp,A,A);
	KSPSetFromOptions(ksp);
	
	KSPSolve(ksp,x,y);
	
	VecScale(y,(dx*dx*dx*dx));
	VecView(y,PETSC_VIEWER_STDOUT_WORLD);


	KSPDestroy(&ksp);
	VecDestroy(&x);
	VecDestroy(&y);
	MatDestroy(&A);

	PetscFinalize();
	return 0;

}
