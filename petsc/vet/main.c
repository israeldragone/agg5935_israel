#include <petsc/petscksp.h>

int main(int argc, char **argv){

	PetscInitialize(&argc,&argv,NULL,NULL);


	Vec x,y,u,v;
	int n=10;
	
	VecCreate(MPI_COMM_WORLD,&x);
	VecSetSizes(x,PETSC_DECIDE,n);
	VecSetFromOptions(x);
	
	VecSet(x,1.3);
	
	int xi,xf,i;
	float a=1.,b=2.,c=4.;
	VecGetOwnershipRange(x,&xi,&xf);
	PetscScalar val,ind;
	
	for (i=xi;i<xf;i++){
		val = (float)1;
		VecSetValues(x,1,&i,&val,INSERT_VALUES);
	}
	
	VecAssemblyBegin(x);
	VecAssemblyEnd(x);

	VecDuplicate(x,&y);
	VecDuplicate(x,&u);
	VecDuplicate(x,&v);
	
	VecAXPY(y,2.0,x);
	VecAXPY(u,1.0,x);
	VecAXPY(v,2.0,x);

	//f(x) = a*x^3 + b*x^2 +c
	VecPow(u,3); //x^3
	VecPow(v,2); //x^2
	VecAXPBY(u,a,b,v); //ax^3+bx^2
	VecShift(u,c); // ax^3+bx^2+c

	VecView(u,PETSC_VIEWER_STDOUT_WORLD);
	
	VecDestroy(&x);

	PetscFinalize();
	return 0;

}
