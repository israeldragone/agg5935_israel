import numpy as np
import matplotlib.pyplot as plt

A = np.loadtxt("orbitas.txt",skiprows=1);

n,m = np.shape(A)

print(n,m)


for i in range(0,n,1):
	
	plt.figure(figsize=(10,10))
	
	sun = A[i,1:3]
	bodies = A[i,3:]

	x,y = sun.reshape(((2)//2,2)).T
	plt.plot(x,y,"*r",ms=7)

	x,y = bodies.reshape(((m-3)//2,2)).T
	plt.plot(x,y,".k")

	plt.xlim([-1.5,1.5])
	plt.ylim([-1.5,1.5])


	plt.savefig("fig_%04d" % (i))
	plt.close()

