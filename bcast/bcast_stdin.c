#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


int main () {

	MPI_Init(NULL,NULL);

	int ID,nproc,k;

	MPI_Comm_rank(MPI_COMM_WORLD,&ID);
	MPI_Comm_size(MPI_COMM_WORLD,&nproc);



	if (ID==0) {

		printf("Escreva um inteiro:\n");
		scanf("%d",&k);
	}


	MPI_Bcast(&k,1,MPI_INT,0,MPI_COMM_WORLD);
	
	printf("%d de %d\n", k, ID);


	MPI_Finalize();
	return 0;


}
