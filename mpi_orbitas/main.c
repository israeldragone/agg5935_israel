#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <stddef.h>

const double G = 6.67E-11;
const double ANO = 365.*24.*3600.;
const double UA = 149597870700.; 

typedef struct corpo{
	double vx;
	double vy;
	double px;
	double py;
	double px2;
	double py2;
	double M;
} Corpo;

typedef struct {

	float a[7];

} str1;



double randNorm()
{
	return(2.0*((double)rand()/RAND_MAX-0.5));
}


void print_orbita(FILE *fo, int n, double t, Corpo *C)
{
	int i;
	
	fprintf(fo,"%f ",t/ANO);
	
	for (i=0;i<n;i++){
		fprintf(fo,"%.3f %.3f ",C[i].px/UA,C[i].py/UA);
	}
	fprintf(fo,"\n");
	
}

//nao precisa mecher
void init(int n, Corpo *C){
	int i;
	
	double x,y;
	double ra, va;
	
	srand(1);
	
	for (i=0; i<n; i++){
		x = randNorm()*200E9;
		y = randNorm()*200E9;
		C[i].px = x;
		C[i].py = y;
		
		ra = sqrt(x*x+y*y);
		
		va = 30000.0*sqrt(150.E9/ra);
		
		C[i].vx = -va*y/ra;//randNorm()*10000;
		C[i].vy = va*x/ra;//randNorm()*10000;
		
		C[i].M = fabs(randNorm()*1E23);
		
	}
	
	//sol
	C[0].vx = 0;
	C[0].vy = 0;
	C[0].px = 0;
	C[0].py = 0;
	C[0].M = 2.0E30;
	
	//terra
	C[1].vx = 0;
	C[1].vy = 30000.0;
	C[1].px = UA;
	C[1].py = 0;
	C[1].M = 6.0E27;
	
	//lua
	if (n>2){
		C[2].vx = 0;
		C[2].vy = 30000.0 + 6800.0;
		C[2].px = UA+400000000.0*15;
		C[2].py = 0;
		C[2].M = 7.0E22;
	}
	
}

void gravidade(int ilocal, int n_loc, int n, double dt, double t, Corpo *C){
	
	int i,j;
	double ax,ay,dx,dy,raux,r3;
	
	for (i=ilocal;i<n_loc;i++){
		ax = 0;
		ay = 0;
		
		//determine a aceleracao ax e ay
		for (j=0;j<n;j++){
		
			if (i == j) continue; 

			dx= (C[i].px-C[j].px);
			dy= (C[i].py-C[j].py);
			
			raux = sqrt( dx*dx + dy*dy );
			r3 = (raux*raux*raux);
			
			ax += -(G*C[j].M*dx) / r3;
			ay += -(G*C[j].M*dy) / r3;

		}
		
		
		//atualiza a velocidade considerando que ela esteja meio passo de tempo deslocado do tempo
		if (t==dt){
			C[i].vx += ax*dt/2;
			C[i].vy += ay*dt/2;
		}
		else {
			C[i].vx += ax*dt;
			C[i].vy += ay*dt;
		}
		
		//atualize os valores de px2 e py2
		C[i].px2 = C[i].px + C[i].vx*dt;
		C[i].py2 = C[i].py + C[i].vy*dt;
	}
	
	for (i=ilocal;i<n_loc;i++){
		C[i].px=C[i].px2;
		C[i].py=C[i].py2;
	}
}




int main(int argc, char** argv)
{

	int ID,nproc;
	MPI_Init(NULL,NULL);
	
	MPI_Comm_size(MPI_COMM_WORLD,&nproc);
	MPI_Comm_rank(MPI_COMM_WORLD,&ID);

	double t1,t2,exec_time;
	MPI_Barrier(MPI_COMM_WORLD);
	t1 = MPI_Wtime();

	Corpo *C1;
	FILE *fo;

	int n = atoi(argv[1]); //numero de corpos
	
	//calcula o numero de corpos que cada processo sera responsavel
	int nlocal = (n/nproc);
	int ilocal = nlocal*ID;
	
	C1 = (Corpo *) malloc(sizeof(Corpo)*n);
	double t=0,taux=0;
	double dt = ANO/10000;
	int i;

	
	//define a estrutura no mpi
	MPI_Datatype CORPO,tipos[1] = {MPI_DOUBLE};
	int blocos[1]={7};

	MPI_Aint displ[1];


	displ[0] = offsetof(str1,a); 

	MPI_Type_create_struct(1,blocos,displ,tipos,&CORPO);
	MPI_Type_commit(&CORPO);


	
	//o processo zero calcula as condicoes inicias do problema
	if (ID == 0){
	
		init(n,C1);

		char ss[100];
		sprintf(ss,"orbitas.txt");
		fo = fopen(ss,"w");
		fprintf(fo,"%d\n",n);

		print_orbita(fo, n, t, C1);

	}

	//envia as condicoes iniciais para todos os processos
	MPI_Bcast(C1,n,CORPO,0,MPI_COMM_WORLD);


	for (t=dt, taux=dt; t<=ANO*1.0; t+=dt,taux+=dt){
		
		gravidade(ilocal,nlocal*(ID+1),n,dt,t,C1);

	//compartilha os valores encontrados por cada processo para os demais
		for (i=0;i<nproc;i++)

			MPI_Bcast(&C1[i*nlocal],nlocal,CORPO,i,MPI_COMM_WORLD);

		if (ID == 0){
			if (taux>=ANO/200) {
				taux=0.0;
				print_orbita(fo, n, t, C1);
			}
		}
		
	}

	t2 = MPI_Wtime();
	exec_time = t2-t1;

	if (ID==0)
		printf("Execution time: %lf seconds \n", exec_time);
	
	MPI_Type_free(&CORPO);
	MPI_Finalize();
	free(C1);
	return (0);
}

