#include <stdio.h>
#include <mpi.h>

int main(int argc, char* argv[])
{
	int ID, Nproc, n,i;
	
	MPI_Init(&argc,&argv);
	
	MPI_Comm_rank(MPI_COMM_WORLD, &ID);
	MPI_Comm_size(MPI_COMM_WORLD, &Nproc);
	
	n = Nproc;
	
	int a[n],b[n], cont;
	char nome[10];


	FILE *arq;
	
	sprintf(nome,"%d.txt",ID);
	
	arq = fopen(nome,"w");


	fprintf(arq,"a:");
	for (i=0;i<n;i++){
		a[i]=(ID+1)*10+i+1;
		fprintf(arq," %d",a[i]);
	}
	fprintf(arq,"\n");

/*	int ts[]={1,1,1,1},tr[]={1,1,1,1};*/
/*	int ds[]={3,2,1,0},dr[]={0,1,2,3};*/

/*	MPI_Alltoallv(a,ts,ds,MPI_INT,b,tr,dr,MPI_INT,MPI_COMM_WORLD);*/
	

	for (cont=0; cont<Nproc; cont++)
		MPI_Scatter(&a[0],1,MPI_INT,&b[cont],cont,MPI_INT,1,MPI_COMM_WORLD);
	
	fprintf(arq,"b: ");
	for (i=0;i<n;i++){
		fprintf(arq,"%d ",b[i]);
	}
	fprintf(arq,"\n");
	
	fclose(arq);
	
	MPI_Finalize();
	return 0;
}
