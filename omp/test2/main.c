#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
int main(int argc, char* argv[])
{
	int n_th = atoi(argv[1]);
	int a=0;
	# pragma omp parallel num_threads(n_th)
	{
		int b=0;
		int meu_th = omp_get_thread_num();
/*		int total_th = omp_get_num_threads();*/
		if (meu_th==0) {
		a=2;
		b=3;
	}
	#pragma omp barrier
	printf("th: %d: a=%d, b=%d\n",meu_th,a,b);
	}
	return 0;
}
