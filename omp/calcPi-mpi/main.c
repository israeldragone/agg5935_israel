#include <stdio.h>
#include <stdlib.h>
#include <mpi/mpi.h>

int main(int argc, char **argv)
{
	int i,n,cont_dentro=0,soma;
	
	int ID, Procs;
	
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&ID);
	MPI_Comm_size(MPI_COMM_WORLD,&Procs);
	
	n = atoi(argv[1]);
	
	float x,y;
	
	srand(ID);
	double t1,t2,dt,tempo_max;
	
	t1 = MPI_Wtime();
	
	
	int n_local = n/Procs;
	
	for (i=0;i<n_local;i++){
		x = (float)(rand())/RAND_MAX;
		y = (float)(rand())/RAND_MAX;
		if (x*x+y*y<1) cont_dentro++;
	}
	
	MPI_Reduce(&cont_dentro,&soma,1,MPI_INT,MPI_SUM, 0, MPI_COMM_WORLD);
	
	if (ID==0) printf("Valor aproximado para pi: %f\n",(float)(soma)*4.0/n);

	t2 = MPI_Wtime();
	
	dt = t2-t1;
	
	MPI_Reduce(&dt,&tempo_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
	
	if (ID==0) printf("%lf segundos\n", tempo_max);
	
	MPI_Finalize();
	
	return 0;
	
}
