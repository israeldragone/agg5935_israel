#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void Trap(double x1, double x2, int cont, double dx, double *total);
double f( double x);

int main(int argc, char *argv[])
{
	
	int n = 1024;
	double a=0.0, b=3.0, dx;
	double soma_total=0;
	int num_th = atoi(argv[1]);
	
	dx = (b-a)/n;

	#pragma omp parallel num_threads(num_th)
	Trap(a,b,n,dx,&soma_total);
	
	
	printf("soma total = %lf\n",soma_total);
	
	return 0;
}

void Trap(double x1, double x2, int cont, double dx, double *total)
{

	int meu_th = omp_get_thread_num();
	int total_th = omp_get_num_threads();
	
	int local_n = cont/total_th;
	double local_x1 = x1+meu_th*local_n*dx;
	double local_x2 = local_x1+local_n*dx;

	double soma, x;
	int i;
	
	soma = (f(local_x1) + f(local_x2))/2.0;
	
	for (i=1;i<local_n;i++){
		x = local_x1 + i*dx;
		soma += f(x);
	}
	soma *= dx;

	#pragma omp critical
	*total += soma;
}


double f( double x)
{
	return(x*x*x);
}

