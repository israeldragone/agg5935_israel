#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

int main(int argc , char **argv)
{
	printf("Hello man!\n");
	int n_th = atoi(argv[1]);

#pragma omp parallel num_threads(n_th) 
	{
	int meu_th = omp_get_thread_num();
	int total_th = omp_get_num_threads();
	printf("Hello, from process %d of %d \n",meu_th, total_th);
	}
	return 0;
}
