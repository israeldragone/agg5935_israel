#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv)
{
	float soma;
	
	long n = atoi(argv[2]); //numero de pontos
	int n_th = atoi(argv[1]); //numero de threads
	
	float x,y;
	double t1,t2,dt;
	
	t1 = omp_get_wtime();
	
	long n_local = n/n_th;
	

	# pragma omp parallel num_threads(n_th)
		{
		int ID = omp_get_thread_num();
		long i, cont_dentro=0;
		unsigned int seed = ID; 

		for (i=0;i<n_local;i++){
			x = (float)(rand_r(&seed))/RAND_MAX;
			y = (float)(rand_r(&seed))/RAND_MAX;
			if (x*x+y*y<1) cont_dentro++;
		}

		#pragma omp critical
		soma += cont_dentro;
	}//fecha pragma


	printf("Valor aproximado para pi: %f\n",(float)(soma)*4.0/n);

	t2 = omp_get_wtime();
	
	dt = t2-t1;
	
	
	printf("%lf segundos\n", dt);
	
	
	return 0;
	
}
