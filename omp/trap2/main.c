#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

double Trap(double x1, double x2, int cont, double dx, int num_th);
double f( double x);

int main(int argc, char *argv[])
{
	
	int n = 1024;
	double a=0.0, b=3.0, dx;
	double soma_total=0;
	int num_th = atoi(argv[1]);
	
	dx = (b-a)/n;

	#pragma omp parallel num_threads(num_th)
	soma_total=Trap(a,b,n,dx,num_th);
	
	
	printf("soma total = %lf\n",soma_total);
	
	return 0;
}

double Trap(double x1, double x2, int cont, double dx, int num_th)
{

	double x,soma;
	int i;
	
	soma = (f(x1) + f(x2))/2.0;
	#pragma omp parallel for num_threads(num_th) reduction(+:soma) private (x)
	for (i=1;i<cont;i++){
		x = x1 + i*dx;
		soma += f(x);
	}
	soma *= dx;

/*	#pragma omp critical*/
/*	*total += soma;*/

	return soma;
}


double f( double x)
{
	return(x*x*x);
}

