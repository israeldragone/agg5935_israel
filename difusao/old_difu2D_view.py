import numpy as np
import matplotlib.pyplot as plt

i=0
for cont in range(0,3900,100):
	A = np.loadtxt("t_%d.txt" % (cont),skiprows=1)

	plt.imshow(A,vmin=0,vmax=11.0)

	plt.savefig("fig_%04d" % (i))
	i+=1