#include <mpi/mpi.h>
#include <stdio.h>
#include <stdlib.h>

void plota(double *T, long contador, double t, long nx, long ny, long ID,long nproc)
{
	int i,j;
	FILE *saida1;
	char nome1[50];
	sprintf(nome1,"t_%ld_%ld.txt",contador,ID);
	saida1 = fopen(nome1,"w");
	//fprintf(saida1,"%lf %ld %ld\n",t,nx,ny);
	int i_init=0,i_final=nx;
	if (ID>0) i_init = 1;
	if (ID<nproc-1) i_final--;
	for (i=i_init;i<i_final;i++){
		for (j=0;j<ny;j++){
			fprintf(saida1,"%4.1lf ",T[i*ny+j]);
		}
		fprintf(saida1,"\n");
	}
	fclose(saida1);
}


int main()
{
	MPI_Init(NULL,NULL);
	
	int ID,nproc;
	
	MPI_Comm_size(MPI_COMM_WORLD,&nproc);
	MPI_Comm_rank(MPI_COMM_WORLD,&ID);

	double t1,t2;
	
	MPI_Barrier(MPI_COMM_WORLD);
	t1 = MPI_Wtime();
	
	double *T = NULL;
	double *T2 = NULL;
	
	double *uf, *up;
	
	long nx=120;
	long ny=nx;
	
	long nx_local = nx/nproc;//+(ID>0)+(ID<nproc-1);
	if (ID>0) nx_local++;
	if (ID<nproc-1) nx_local++;
	
	fprintf(stderr,"%d %ld\n",ID,nx_local);
	
	
	
	double L=1.0;
	
	double dx = L/(nx-1);
	
	double difu = 1.0;

	double dt = (dx*dx/4.0/difu)/2.0; //metade do limite de convergencia
	double r_fac = difu*dt/(dx*dx);
	double t=0,t_max = 1;
	
	long i,j,k,cont=0,i0=(nx/nproc)*ID;
	
	if (ID>0) i0--;
	
	T = (double *)calloc(nx_local*ny,sizeof(double));//aloca e zera
	T2 = (double *)calloc(nx_local*ny,sizeof(double));//aloca e zera
	
	double xaux,yaux;
	
	for (i=0;i<nx_local;i++){
		for (j=0;j<ny;j++){
			xaux = (i+i0)*L/(nx-1);
			yaux = j*L/(ny-1);
			
			if (xaux>1./3. && xaux<2./3. && yaux>1./3. && yaux<2./3.) T[i*ny+j]=10.;
		}
	}
	plota(T,cont,t,nx_local,ny,ID,nproc);
	
	uf = &T2[0];
	up = &T[0];
	
	int tag = 1;
	int posic, posic2, vizinho;
	while (t<t_max) {
		
		//atualize uf em funcao dos valores de up
		for (i=1;i<nx_local-1;i++){
			for (j=1;j<ny-1;j++){
				k = i*ny+j;
				uf[k]=up[k]+r_fac*(up[k+ny]+up[k-ny]+up[k+1]+up[k-1]-4*up[k]);
			}
		}
		
		///////troca de linhas
		
		for (int verif=0;verif<=1;verif++){
		
			if(ID%2==verif) { //
				posic=ny; 
				posic2=0;
				vizinho=ID-1;
			}
		
			else {
				posic=(nx_local-2)*ny; 
				posic2=(nx_local-1)*ny;
				vizinho=ID+1;
			}
		
			if(vizinho>=0 && vizinho<nproc){
		
				MPI_Sendrecv(&uf[posic],ny,MPI_DOUBLE,vizinho,tag,&uf[posic2],ny,MPI_DOUBLE,
				vizinho,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			}
			
		} // fecha for
		
		
		if (cont%2==0) {
			uf = &T[0];
			up = &T2[0];
		}
		else {
			uf = &T2[0];
			up = &T[0];
		}
		t+=dt;cont++;
		if (cont%100==0) plota(T,cont,t,nx_local,ny,ID,nproc);
	}
	
	free(T2);
	free(T);

	
	t2 = MPI_Wtime();

	double dif_tempo = t2-t1, diff_max;
	
	MPI_Reduce(&dif_tempo,&diff_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

	if (ID==0)
		printf("tempo de execucao: %lf s\n",dif_tempo);
	
	MPI_Finalize();

	return(0);


}
