import numpy as np
import matplotlib.pyplot as plt
from subprocess import call





i=0
for cont in range(0,400,100):
	
	call("cat t_%d_*.txt > t_%d.txt" % (cont,cont),shell=True)
	
	A = np.loadtxt("t_%d.txt" % (cont),skiprows=1)

	plt.imshow(A,vmin=0,vmax=11.0)

	plt.savefig("fig_%04d" % (i))
	i+=1
