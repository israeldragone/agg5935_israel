#include <stdio.h>
#include <stdlib.h>

void plota(double *T, long contador, double t, long nx, long ny)
{
	int i,j;
	FILE *saida1;
	char nome1[50];
	sprintf(nome1,"t_%ld.txt",contador);
	saida1 = fopen(nome1,"w");
	fprintf(saida1,"%lf %ld %ld\n",t,nx,ny);
	for (i=0;i<nx;i++){
		for (j=0;j<ny;j++){
			fprintf(saida1,"%4.1lf ",T[i*ny+j]);
		}
		fprintf(saida1,"\n");
	}
	fclose(saida1);
}


int main()
{
	double *T = NULL;
	double *T2 = NULL;
	
	double *uf, *up;
	
	long nx=1000;
	long ny=nx;
	
	double L=1.0;
	
	double dx = L/(nx-1);
	
	double difu = 1.0;

	double dt = (dx*dx/4.0/difu)/2.0; //metade do limite de convergencia
	double r_fac = difu*dt/(dx*dx);
	double t=0,t_max = 0.01;
	
	long i,j,k,cont=0;
	
	T = (double *)calloc(nx*ny,sizeof(double));//aloca e zera
	T2 = (double *)calloc(nx*ny,sizeof(double));//aloca e zera
	
	double xaux,yaux;
	
	for (i=0;i<nx;i++){
		for (j=0;j<ny;j++){
			xaux = i*L/(nx-1);
			yaux = j*L/(ny-1);
			
			if (xaux>1./3. && xaux<2./3. && yaux>1./3. && yaux<2./3.) T[i*ny+j]=10.;
		}
	}
	plota(T,cont,t,nx,ny);
	
	uf = &T2[0];
	up = &T[0];
	
	
	while (t<t_max) {
		
		//atualize uf em funcao dos valores de up

		for (i=1; i<nx-1; i++){
			for (j=1;j<ny-1;j++){
				k = i*ny+j;
				uf[k] = up[k] + r_fac*(up[k+ny]+up[k-ny]+up[k+1]+up[k-1]-4*up[k]);
			}
		}
		
		if (cont%2==0) { // troca os ponteiros para variar entre o Tf e o Tp p/ nao precisar copiar
			uf = &T[0];
			up = &T2[0];

		}
		else {
			uf = &T2[0];
			up = &T[0];
		}
		t+=dt;cont++;
		if (cont%100==0) plota(T,cont,t,nx,ny);
	}
	
	free(T2);
	free(T);

	return (0);


}
