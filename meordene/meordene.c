#include <stdio.h>
#include <stdlib.h>

void show(char *prefix, int *dados, int n) {
	int i;

	fprintf(stdout,"%s", prefix);
	for(i = 0; i < n; i++) {
		fprintf(stdout,"%d ",dados[i]);
	}
	fprintf(stdout,"\n");

	return;
}


int comparUp (const void *x, const void *y){

	int a = *((int*) x);
	int b = *((int*) y);

	if (a > b)
		return 1;

	else if (a < b)
		return -1;

	else 
		return 0;

}

int comparDown (const void *x, const void *y){

	int a = *((int*) x);
	int b = *((int*) y);

	if (a > b)
		return -1;

	else if (a < b)
		return 1;

	else 
		return 0;

}

int comparOddEven (const void *x, const void *y){

	int a = *((int*) x);
	int b = *((int*) y);

	if (a%2 > b%2)
		return 1;

	else if (a%2 < b%2)
		return -1;

	else 
		return 0;

}

int comparOddEvenUpDown (const void *x, const void *y){

	int a = *((int*) x);
	int b = *((int*) y);

	if (a%2 > b%2)
		return -1;

	else if (a%2 < b%2)
		return 1;

	else {

		if (a%2 == 0) 
			return comparUp(&a,&b);

		else
			return comparDown(&a,&b);
	}

}

int main(int argc, char *argv[]) {
	int dados[] = {1, 123, 3, 5, 2, 4, 15, 9, 10, 23, 43};
	int n = 11;


	// Before order
	show("ORI: ", dados, n);

	// Sort Ascending
	qsort (dados, n, sizeof(int), comparUp);

	// After Sort
	show("ASC: ", dados, n);

	// Sort Descending
	qsort (dados, n, sizeof(int), comparDown);

	// After Sort
	show("DES: ", dados, n);

	// Sort Odd vs Even
	qsort (dados, n, sizeof(int), comparOddEven);

	// After Sort
	show(" OE: ", dados, n);

	// Sub Odd -> descending / Even -> ascending !
	qsort (dados, n, sizeof(int), comparOddEvenUpDown);


	// After Sort
	show("OES: ", dados, n);
		
	return 0;
}
