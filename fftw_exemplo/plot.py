import numpy as np
import matplotlib.pyplot as plt
import sys

t,x,f,am,ph,r = np.loadtxt(sys.argv[1],unpack=True)

plt.plot(t,x,"-")
plt.plot(t,r,"p")
plt.show()

plt.plot(f,am)
plt.show()

plt.plot(f,ph,"-o")
plt.show()
