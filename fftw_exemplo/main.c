#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// Adicionar os includes necessários 
// incluindo sempre antes do complex.h
// do c99

#include <complex.h>
#include <fftw3.h>

int main(int argc, char **argv) {
	long i;
	
	// Parâmetros de entrada
	if (argc < 3) {
		fprintf(stderr, "Você deve passar o número de amostras, o intervalo de amostragem e uma frequencia\n");
		return 1;
	}
	long  nmax = atol(argv[1]);
	float   dt = atof(argv[2]);
	float freq = atof(argv[3]);
	
	// Definição das variáveis e alocação de memória
	
    fftw_complex *input;
    fftw_complex *output;
    fftw_complex *output_inverse;
	
	input = fftw_alloc_complex(nmax);
	output = fftw_alloc_complex(nmax);
    output_inverse = fftw_alloc_complex(nmax);
	
	// Cálculo de Plano
	//
	// O cálculo do plano deve sempre ser feito
	// antes, pois, ele destrói os ponteiros dados !
	//
	
    fftw_plan direct = fftw_plan_dft_1d(nmax, input, output, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan inverse = fftw_plan_dft_1d(nmax, output, output_inverse, FFTW_BACKWARD, FFTW_ESTIMATE);
	
	// Cálculo da função a ser transformada
	// por exemplo um f(x) = sin(2*pi*f*t) = sin(2*pi*f*i*dt)
	for (i=0; i < nmax; i++) {
        input[i] = sin(2*M_PI*freq*i*dt) + 0.0*I;
	}
	
	// Executar a transformada
	
    fftw_execute(direct);
    fftw_execute(inverse);

	// Imprimir a saida
	float df = 1.0/(dt * nmax);
	for(i=0;i<nmax;i++) {
		fprintf(stdout, "%lf %lf %lf %lf %lf %lf\n",
				(float)(i*dt), // Os tempos onde f(t) foram calculados
                creal(input[i]),       // Os valores de f(t)
                (float)i*df,   // Os valores de frequencia
                cabs(output[i]/nmax),        // O espectro de amplitude
                carg(output[i]),        // O espectro de fase
                creal(output_inverse[i]/nmax)        // Os valores transnformados novamente
		);
	}
	
	// Limpar a memória
    fftw_destroy_plan(direct);
    fftw_destroy_plan(inverse);


    fftw_free(input);
    fftw_free(output);
    fftw_free(output_inverse);
	
	// Terminar o código
	return 0;
}
