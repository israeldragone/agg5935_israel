#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG 1

/*
 * Programa para ler parte da saida de um servidor FDSNws em modo texto, pode testar da seguinte maneira:
 * 
 * wget -q -O - 'http://www.moho.iag.usp.br/fdsnws/event/1/query?format=text&limit=10' | main
 */

int readone(FILE *fio) {
	/*
	 * Le uma linha, retorna:
	 * 
	 * 1  = Quando lido
	 * 0  = Se a linha for comentário
	 * -1 = Se o arquivo acabar
	 * -2 = Se não tem os campos esperados
	 */
	
	char line[1024];
	
	char *s_date, *s_id, *s_lat, *s_lon, *s_depth;
	float lat, lon, depth;
	
	if ((fgets(line, 1024, fio)) == NULL) return -1;
	if (line[0] == '#') return 0;
	
	s_id   = strtok(line, "|");
	s_date = strtok(NULL, "|");
	s_lat  = strtok(NULL, "|");
	s_lon  = strtok(NULL, "|");
	s_depth= strtok(NULL, "|");
	
	if (s_id == NULL || s_date == NULL || s_lat == NULL || s_lon == NULL || s_depth== NULL) return -2;
	
	s_id   = strdup(s_id);
	s_date = strdup(s_date);
	lat    = atof(s_lat);
	lon    = atof(s_lon);
	depth  = atof(s_depth);
	
	if (DEBUG) fprintf(stderr, "%s %s %f %f %f\n",s_id, s_date, lat, lon, depth);
	
	free(s_id);
	free(s_date);
	
	return 1;
}

int main (int argc, char **argv) {
	
	// Leia da entrada padrão !
	while (readone(stdin) >= 0);
	
	return 0;
}
