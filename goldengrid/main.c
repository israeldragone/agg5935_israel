#include <stdio.h>
#include <stdlib.h>


typedef struct golden_grid {
	char id[4];
	short nx;
	short ny;
	double xmin;
	double xmax; 
	double ymin;
	double ymax;
	double zmin;
	double zmax;
} GOLDENHEADER;


int main (int argc, char **argv) {

	if (argc != 2) {
		printf("execute ./gd filename\n");
		return 1;
	}


	FILE *fio;
	GOLDENHEADER *h = NULL ;
	float *z = NULL, y;
	double dx, dy;
	char *filename = argv[1];
	int i,j, k=0, l=0;

	h = malloc(sizeof(GOLDENHEADER));

	if (h == NULL) {
		printf("h null");
		return 1;
	}

	if  (( fio = fopen(filename,"rb")) == NULL ){
		fprintf(stdout, "Arquivo invalido \n");
		return 1;
	}

	k=fread(h,sizeof(GOLDENHEADER),1, fio);

	if (k != 1) {
		printf("k igual a %d, a leitura do cabecalho nao funcionou\n\n",k);
		return 1;
	}

	dx = (h->xmax - h->xmin) / h->nx;
	dy = (h->ymax - h->ymin) / h->ny;

	if (h->nx > h->ny) {
		z=(float *)malloc(sizeof(float)*h->nx);
		l=fread(z,sizeof(float)*h->nx,1,fio);
	}

	else {
	z=malloc(sizeof(float)*h->nx*h->ny);
	l=fread(z,sizeof(z),1,fio);
	}


	if (l != 1) {
		printf("l igual a %d, a leitura dos dados nao funcionou\n\n",k);
		return 1;
	}

	fclose(fio);

/*	fprintf(stdout, "%lf, %lf, %lf, %lf, %lf, %lf,", h->xmin,h->xmax, h->ymin, h->ymax, h->zmin, h->zmax);*/

	fprintf(stdout,"%lf %hi %lf \n%lf %hi %lf\n", h->xmin, h->nx, dx, h->ymin, h->ny, dy);

	for(i=0;i<h->ny;i++) {

		for(j=0;j<h->nx;j++)

			fprintf(stdout,"%f \t", z[i]);

		fprintf(stdout,"\n");
	}

	free(h);
	free(z);
	return 0;

}

