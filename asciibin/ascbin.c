#include <stdio.h>

int main (int argc, char **argv){
	
	float seq; 
	int rcount=0;

	while ( !feof(stdin) ){
		rcount=fscanf(stdin, "%f", &seq);
		if (rcount != 1) continue;
		fwrite(&seq, sizeof (float), 1, stdout);
	}

	return 0;
}
