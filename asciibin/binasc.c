#include <stdio.h>

int main (int argc, char **argv){
	
	float seq=0;
	int count = 0;

	while ( !feof(stdin) ){
		count=fread(&seq, sizeof (float), 1, stdin);
		if (count != 1) continue;
		fprintf(stdout,"%f\n", seq);
	}

	return 0;
}
