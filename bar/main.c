#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define nchar 50

void makebar (char *message, int current) {

	int i = 0;

	printf("\r%s ", message);
	
	for (i=0; i<= current; i++){
		printf("-");
		if (i == current)
			printf(">");
	}
	
	for (i=current; i<=nchar; i++){
		printf(" ");
	}
	printf("%d %%", current*100/nchar);
}

int main(int argc, char **argv) {
	stdout = stderr;
	// Parsing
	char *str = argv[1];
	int start = atoi(argv[2]);
	int stop = atoi(argv[3]);
	int step = atoi(argv[4]);
	int i = 0, sleep = 0;

	//validate
	if (argc != 5) {
		printf("\nNumero de argumentos inválido\n");
		return 1;
	}
	
	if ( (stop <= start) || (step > stop) ) {
		printf("\nValores invalidos, rode novamente \n");
		return 1;
	} 

	int value;

	sleep=(stop/nchar)*100000;

	for (value=start; value<=nchar; value+=step ){
		makebar (str, value);
		usleep(sleep);
	}

	printf("\n");
	return 0;
}
